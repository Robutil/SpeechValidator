# Live version
Available at: [validator.robbit.me](https://validator.robbit.me)

# Short explanation
WER -> Word Error Rate -> number of errors / max matches
WRR -> Word success rate -> number of successes / max matches

# API
api requests must be made to:

[api.robbit.me](http://api.robbit.me)

Put the JSON in the content header of the HTML POST request.

After doing an API request, described underneath, you can expect something 
like this:

```
{
  "wrr": 0.8888888888888888,
  "wer": 0.1111111111111111,
  "hypothesis": "their quick brown fox jumped over the lazy dog",
  "reference": "the quick brown fox jumped over the lazy dog",
  "reference_checked": "THE quick brown fox jumped over the lazy dog",
  "hypothesis_checked": "THEIR quick brown fox jumped over the lazy dog",
  "insertions": 0,
  "deletions": 0,
  "replacements": 1,
  "total_errors": 1,
  "total_matches": 8,
  "total_length": 9
}
```

In the example above:
WER = 1 / 9 = 0.1111
WRR = 8 / 9 = 0.8889

The most important number is wer, which stands for Word Error Rate. This 
number shows how succesful the hypothesis was, lower is better. Do not 
be confused by the word hypothesis, it is the scientific term for a guess. 
The wrr is the succes rate, with a succes rate of 1 being perfect. The other
data is analysis. The reference_checked and hypothosis_checked show what went
wrong, BOLD words show wrongs and **** words show missing items. The other 
items are self-explenatory. 

##  Disclaimer
The reference_checked and hypothesis_checked is just there to provide an 
idea of what mistakes were found, this is not always accurate. The WER
and WRR are always accurate, even if the checked values seem odd.

## Usage
api requests must be made to:

api.robbit.me

Put the JSON in the content header of the HTML POST request.

There multiple requests possible, requesting single sentences for validation
and multiple (arrays).

For requesting single sentences, create a JSON string with the following
fields:

```
{
  "guess": "stringtype",
  "reference": "stringtype",
}
```

The validator will then check the guess against the reference. If you are using
one of the samples, you can simply provide the sample number index (with first
sample at i=0) instead. The packet will then look like this:

```
{
  "guess": "stringtype",
  "sample_id": 0
}
```

A single request is not always desirable, therefore it is also possible to
request validation on an entire array. Like this:

```
{
  "guesses": ["stringtype", "stringtype", "stringtype"]
  "references": ["stringtype", "stringtype", "stringtype"]
}
```

Here guess[0] is validated against reference[0] and so forth. Once again you 
can also just supply the sample id numbers if you are using the samples, 
like so:

```
{
  "guesses": ["stringtype", "stringtype", "stringtype"]
  "sample_ids": [0, 1, 2]
}
```

In fact, you can leave out the sample ids alltogether when using arrays, and 
the validator will assume that the first (index=0) guess corresponds with the 
first sample. So the packet underneath is thesame as the packet above:

```
{
  "guesses": ["stringtype", "stringtype", "stringtype"]
}
```

Note that the sample id cannot exceed 5, and when using a guesses only array 
you can only store a total of 6 guesses (0..5). But, if you provide the 
sample_id's you can pump as many requests as you want. 

# Running stand-alone
requires python3 with flask, flash-restful and edit_distance
install using pip3

For debugging only, requirement:
pip3 install asr-evaluation
