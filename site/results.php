<?php
$data = json_decode($_GET['json']);

$hypothesis_checked = explode(" ", $data->hypothesis_checked);
$reference = explode(" ", $data->reference);


$searchString = '*';
foreach ($hypothesis_checked as $key => $word) {
	if( strpos($word, $searchString) !== false ) {
		$words[$key] = "<font color='red'><u>".$reference[$key]."</u></font>";
	} else if(preg_match('/[A-Z]/', $word)) {
		$words[$key] = "<font color='red'><u>".$reference[$key]."</u></font>";
	} else if(!empty($reference[$key])){
		$words[$key] = $word;
	}
}

$corrected = implode(" ", $words);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Speech validetor - Results</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="icon" href="icon/Microphone-48-icon.png" sizes="48x48" type="image/png">
</head>
<body>
	<nav>
		<div class="item">Speech validator</div>
	</nav>
	<main>

		<a href="index.php"><button> << Nieuwe vergelijking</button></a>
		<div class="summary">
			<p>WRR score: <?php echo $data->wrr; ?></p>
			<p>WER score: <?php echo $data->wer; ?></p>
			<p>Totaal aantal fouten: <?php echo $data->total_errors; ?>
			<p>Verkeerd: <?php echo $data->replacements; ?></p>
			<p>Weg gelaaten: <?php echo $data->deletions; ?></p>
			<p>Toegevoegd: <?php echo $data->insertions; ?></p>
		</div>
		<div class="result">
			<div>Orginele tekst</div>
			<div>Herkende tekst</div>
			<div><?php echo $corrected; ?></div>
			<div><?php echo $data->hypothesis; ?></div>
		</div>
	</main>
	<footer>
		<ul>
			<a target="_blank" href="http://robbit.me"><li title="created by Robbin Siepman and Wiebe Stassen">&copy; robbit.me</li></a>|
			<a target="_blank" href="https://gitlab.com/Robutil/SpeechValidator/blob/master/how_to_use_api.txt"><li class="gitlab">API</li></a>|
			<a target="_blank" href="https://gitlab.com/Robutil/SpeechValidator/blob/master/readme.txt"><li class="gitlab">API documentation</li></a>|
			<a target="_blank" href="https://gitlab.com/Robutil/SpeechValidator"><li class="gitlab">Gitlab</li></a>|
			<a target="_blank" href="disclamer.html"><li>Disclamer</li></a>
		</ul>
	</footer>
</body>
</html>