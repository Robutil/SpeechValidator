<?php
if(isset($_POST['send'])) {
	if(!empty($_POST['orginal']) && !empty($_POST['check'])) {
		$orginal = $_POST['orginal'];
		$check = $_POST['check'];

		$url = 'http://api.robbit.me';

		$arr = array();

		$arr['guess'] = $check;
		$arr['reference'] = $orginal;

		$data = json_encode($arr);
		

		// use key 'http' even if you send the request to https://...
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => $data
		    )
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		if ($result === FALSE) {

		} else {
			header("Location: results.php?json=".$result); 
		}

	}
	
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Speech validator - Home</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="icon" href="icon/Microphone-48-icon.png" sizes="48x48" type="image/png">
</head>
<body>
	<nav>
		<div class="item">Speech validator - Home</div>
	</nav>
	<main>
		<form method="post">
			<label for="orginal">Orginele tekst</label>
			<label for="check">Herkende tekst</label>
			<textarea id="orginal" name="orginal" rows="20"><?php echo $orginal; ?></textarea>
			<textarea id="check" name="check" rows="20"><?php echo $check; ?></textarea>
			<input type="submit" name="send" value="bereken score" />
		</form>
		<p class="note">Geen zin om tekst te kopieren? Maak gebruik de <a class="link" target="_blank" href="https://gitlab.com/Robutil/SpeechValidator/blob/master/how_to_use_api.txt">API</a>. <a class="link" target="_blank" href="https://gitlab.com/Robutil/SpeechValidator/blob/master/readme.txt">API Documentatie</a> en code is te vinden op <a class="link" target="_blank" href="https://gitlab.com/Robutil/SpeechValidator">gitlab</a>
	</main>
	<footer>
		<ul>
			<a target="_blank" href="http://robbit.me"><li title="created by Robbin Siepman and Wiebe Stassen">&copy; robbit.me</li></a>|
			<a target="_blank" href="https://gitlab.com/Robutil/SpeechValidator/blob/master/how_to_use_api.txt"><li class="gitlab">API</li></a>|
			<a target="_blank" href="https://gitlab.com/Robutil/SpeechValidator/blob/master/readme.txt"><li class="gitlab">API documentation</li></a>|
			<a target="_blank" href="https://gitlab.com/Robutil/SpeechValidator"><li class="gitlab">Gitlab</li></a>|
			<a target="_blank" href="disclamer.html"><li>Disclamer</li></a>
		</ul>
	</footer>
</body>
</html>