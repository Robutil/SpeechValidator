import subprocess

from sample_container import SampleContainer

from validator.utils.text_processor import TextProcessor


class IntegrityChecker(object):
    def __init__(self):
        """
        This class was used during the development of the WER algorithm to validate its results.
        Currently this class is no longer in use.
        """
        self.prepper = TextProcessor()
        self.sample_container = SampleContainer()
        self.reference_samples = self.sample_container.get_samples()

    @staticmethod
    def _dump_to_file(name, content):
        with open(name, 'w') as f:
            f.write(content)

    def check(self, guess, reference):
        if isinstance(reference, str):
            return self._check_sentence(guess, reference)
        return None

    @staticmethod
    def find_between(s, first, last):
        try:
            start = s.index(first) + len(first)
            end = s.index(last, start)
            return s[start:end]
        except ValueError:
            return ""

    def _parse_output(self, output_str):
        results = {}
        results['reference'] = self.find_between(output_str, "REF: ", "\nHYP")
        results['hypothesis'] = self.find_between(output_str, "HYP: ", "\nSENTENCE 1")
        output_str = output_str.replace(' ', '')
        print(output_str)
        results['correct'] = float(self.find_between(output_str, 'Correct=', '%'))
        results['errors'] = float(self.find_between(output_str, 'Errors=', '%'))
        results['wer'] = float(self.find_between(output_str, 'WER:', '%'))
        results['wrr'] = float(self.find_between(output_str, 'WRR:', '%'))
        results['ser'] = float(self.find_between(output_str, 'SER:', '%'))

        for key in results:
            print(key + ": " + str(results[key]))

    def _check_sentence(self, sentence, reference):
        sentence = self.prepper.clean_sentence(sentence)
        reference = self.prepper.clean_sentence(reference)

        self._dump_to_file("1.txt", sentence)
        self._dump_to_file("2.txt", reference)
        raw = subprocess.check_output(['wer', '1.txt', '2.txt', '-a', '-e', '-i', '-c']).decode("utf-8")

        self._parse_output(raw)


if __name__ == "__main__":
    wer = IntegrityChecker()
    wer.check("once upon a midnight dreary while i pondered weak and weary over many a quaint and curious volume of",
              "once upon dreary while i pondered eak weak and weay over many a quaint curious volume of")
