import json

from validator.utils.sample_container import SampleContainer
from validator.utils.text_processor import TextProcessor
from validator.utils.word_error_count import WordErrorCount


class TextValidator(object):
    def __init__(self):
        """
        This class is responsible for cleaning text and validating it using the word error count.
        """
        self.prepper = TextProcessor()
        self.checker = WordErrorCount()
        self.sample_container = SampleContainer()

    def validate_against_samples(self, guesses, parse_to_json=True):
        references = self.sample_container.get_samples()

        return self.validate_array(guesses, references, parse_to_json=parse_to_json)

    def validate_array(self, guesses, references, parse_to_json=False):
        if len(guesses) > len(references):
            if parse_to_json:
                return '{}'
            else:
                return []

        results = []
        for k in range(len(guesses)):
            results.append(self.validate(guesses[k], references[k], parse_to_json=False))

        if parse_to_json:
            return json.dumps({'results': results})
        else:
            return results

    def validate_against_sample(self, guess, sample_index, parse_to_json=True):
        sample = self.sample_container.get_sample(sample_index)
        if sample is None:
            return None

        if parse_to_json:
            return json.dumps(self.validate(guess, sample))
        else:
            return self.validate(guess, sample)

    def validate(self, hypothesis, reference, parse_to_json=False):
        hypothesis = self.prepper.clean_sentence(hypothesis)
        reference = self.prepper.clean_sentence(reference)

        if parse_to_json:
            return json.dumps(self.checker.check_sentence(hypothesis, reference))
        else:
            return self.checker.check_sentence(hypothesis, reference)


if __name__ == "__main__":
    # The hypothesises are provided by classmates M and P.
    hypothesises = [
        'the quick brown fox jumped over the lazy dog',
        'first titles won by two speakers the english language can be quite complex it can be understood through '
        'tough thorough thought and',
        'dearest creature in creation study english pronunciation i will teach you in my first dah sound like corpse '
        'corps horse and worse i will keep you suzy busy make your head with heat grow dizzy tear in eye your dress '
        'will tear so shall i oh hear my prayer',
        'what\'s upon a midnight dreary while i pondered weak and wearing over many a quaint and curious volume of '
        'forgotten lore while i nodded nearly napping suddenly there came a tapping as of some one gently rapping '
        'rapping at my chamber door this is some fist around muttered tapping at my chamber door only this and '
        'nothing more',
        'english isn\'t the doors sleeve difficult language to learn because it is so horribly in regular in its '
        'spelling and pronunciation subject to more than one thousand years of external influence the forced '
        'imposition of french is shifted in promotion asian after spelling weeknd fixed the linguistic influences of '
        'the classic klein watches and a huge importance of the foreign words as result of exploration and '
        'colonialism has turned english into michigan',
        'i may be drunk miss but in the morning i\'ll be sober and you will still be ugly'
    ]

    evaluator = TextValidator()
    res = evaluator.validate_against_samples(hypothesises)
    print(res)
    res = evaluator.validate_against_sample(hypothesises[0], 0)
    print(res)
