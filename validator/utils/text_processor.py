import unittest


class TextProcessor(object):
    """
    This class is responsible for cleaning text, it removes capital letters and punctuation marks from the text.
    """
    @staticmethod
    def clean_sentence(messy_sentence):
        clean_sentence = ""
        for character in messy_sentence:
            character = character.lower()
            if character.isalpha() or character == ' ':
                clean_sentence += character
        return clean_sentence

    def clean_array(self, samples):
        clean_samples = []
        for sample in samples:
            clean_samples.append(self.clean_sentence(sample))
        return clean_samples

    def clean(self, target):
        if isinstance(target, list):
            return self.clean_array(target)
        elif isinstance(target, str):
            return self.clean_sentence(target)


class PrepperTest(unittest.TestCase):
    def testVersusReference(self):
        from validator.utils.sample_container import SampleContainer
        sample_container = SampleContainer()

        prepper = TextProcessor()
        for i in range(sample_container.sample_length):
            output = prepper.clean(sample_container.get_raw_sample(i))
            self.assertEqual(output, sample_container.get_sample(i))

    def testVersusDynamic(self):
        prepper = TextProcessor()
        expected_output = [
            'hal l',
            'godby'
        ]
        output = prepper.clean(['Hal l0', 'g0,.;odBy3'])
        self.assertEqual(expected_output, output)


if __name__ == '__main__':
    unittest.main()
