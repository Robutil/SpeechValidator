class SampleContainer(object):
    """
    This class holds the samples used in a school course about Speech Recognition, also used for test cases.
    """
    _raw_samples = [
        'The quick brown fox jumped over the lazy dog.',
        'For those who are not native speakers, the English language can be quite complex. It can be understood '
        'through tough, thorough thought, though.',
        'Dearest creature in creation, study English pronunciation. I will teach you in my verse, sounds like corpse '
        'corps horse and worse. I will keep you, Suzy, busy, make your head with heat grow dizzy. Tear in eye, '
        'your dress will tear. So shall I, oh hear my prayer.',
        'Once upon a midnight dreary, while I pondered, weak and weary, Over many a quaint and curious volume of '
        'forgotten lore. While I nodded, nearly napping, suddenly there came a tapping, As of some one gently rapping, '
        'rapping at my chamber door. Tis some visitor, I muttered, tapping at my chamber door. Only this and nothing '
        'more.',
        'English is notoriously a difficult language to learn because it is so horribly irregular in its spelling and '
        'pronunciation. Subjection to more than a thousand years of external influences; the forced imposition of '
        'French, shifts in pronunciation after spelling became fixed, the linguistic influence of the classical '
        'languages, and a huge importation of foreign words as a result of exploration and colonialism, has turned '
        'English into a mishmash.',
        'I may be drunk, miss, but in the morning I will be sober, and you will still be ugly.'
    ]

    _samples = [
        'the quick brown fox jumped over the lazy dog',
        'for those who are not native speakers the english language can be quite complex it can be understood '
        'through tough thorough thought though',
        'dearest creature in creation study english pronunciation i will teach you in my verse sounds like corpse '
        'corps horse and worse i will keep you suzy busy make your head with heat grow dizzy tear in eye your '
        'dress will tear so shall i oh hear my prayer',
        'once upon a midnight dreary while i pondered weak and weary over many a quaint and curious volume of '
        'forgotten lore while i nodded nearly napping suddenly there came a tapping as of some one gently rapping '
        'rapping at my chamber door tis some visitor i muttered tapping at my chamber door only this and nothing '
        'more',
        'english is notoriously a difficult language to learn because it is so horribly irregular in its spelling '
        'and pronunciation subjection to more than a thousand years of external influences the forced imposition '
        'of french shifts in pronunciation after spelling became fixed the linguistic influence of the classical '
        'languages and a huge importation of foreign words as a result of exploration and colonialism has turned '
        'english into a mishmash',
        'i may be drunk miss but in the morning i will be sober and you will still be ugly'
    ]
    sample_length = len(_samples)
    raw_sample_length = len(_raw_samples)

    def get_sample(self, index):
        if index > len(self._samples):
            return None
        return self._samples[index]

    def get_raw_sample(self, index):
        if index > len(self._raw_samples):
            return None
        return self._raw_samples[index]

    def get_samples(self):
        return list(self._samples)

    def get_raw_samples(self):
        return list(self._raw_samples)
