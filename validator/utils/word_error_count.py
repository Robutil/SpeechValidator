from functools import reduce
from edit_distance import SequenceMatcher


class WordErrorCount(object):
    _opcodes = ['replace', 'delete', 'insert']

    def check_sentence(self, hypothesis, reference):
        if len(reference) == 0:
            return None

        # Convert str to word lists
        hypothesis = hypothesis.split()
        reference = reference.split()

        # Let the builtin sequence matcher find similarities
        seq_matcher = SequenceMatcher(a=reference, b=hypothesis)

        # Parse sequence matcher to matches, replaces, deletes and inserts
        wer_param = self._get_wer_parameters(seq_matcher)

        # Parse WER param to something human readable
        result = self._parse_wer_parameters(wer_param, hypothesis, reference)
        # self._dump_dict(result)
        return result

    def _parse_wer_parameters(self, param, hypothesis, reference):
        result = dict(param)
        result['total_length'] = len(reference)

        hyp, ref = self.format_mistakes(param, hypothesis, reference)

        # Remove clutter
        del result['matches']
        del result['errors']
        del result['error_lengths']
        del result['match_lengths']

        result["wer"] = result['total_errors'] / len(reference)
        result['wrr'] = result['total_matches'] / len(reference)
        result['hypothesis_checked'] = hyp
        result['reference_checked'] = ref
        result['reference'] = ' '.join(reference)
        result['hypothesis'] = ' '.join(hypothesis)

        return result

    def _get_wer_parameters(self, sm):
        opcodes = sm.get_opcodes()

        errors = []
        error_lengths = []
        matches = []
        match_lengths = []
        replacements = deletions = insertions = 0

        for cd in opcodes:
            # Check if distance is relevant for WER
            if cd[0] in self._opcodes:
                if cd[0] == 'replace':
                    replacements += 1
                elif cd[0] == 'delete':
                    deletions += 1
                elif cd[0] == 'insert':
                    insertions += 1

                errors.append(cd)
                error_lengths.append(max(cd[2] - cd[1], cd[4] - cd[3]))

            elif cd[0] == 'equal':
                matches.append(cd)
                match_lengths.append(max(cd[2] - cd[1], cd[4] - cd[3]))

        return {
            "deletions": deletions,
            "insertions": insertions,
            "replacements": replacements,
            "errors": errors,
            "error_lengths": error_lengths,
            "total_errors": reduce(lambda x, y: x + y, error_lengths, 0),
            "matches": matches,
            "match_lengths": match_lengths,
            "total_matches": reduce(lambda x, y: x + y, match_lengths, 0)
        }

    def format_mistakes(self, param, hypothesis, reference):
        ref_list = self._create_log_reference(param, hypothesis, reference)
        hyp_list = self._create_log_hypothesis(param, hypothesis, reference)
        return ' '.join(hyp_list), ' '.join(ref_list)

    @staticmethod
    def _create_log_shared(param, mine, target, my_address, target_address, method):
        counter = 0
        my_list = []

        for err in param['errors']:
            index = err[my_address]

            my_list.extend(mine[counter:index])

            if err[0] == method:
                if len(my_list) == 0 or '*' not in my_list[-1]:
                    my_list.append(mine[index])
                my_list.append('*' * len(target[err[target_address]]))
            else:
                my_list.append(mine[index].upper())
            counter = index + 1

        my_list.extend(mine[counter:len(mine)])
        return my_list

    def _create_log_reference(self, param, hypothesis, reference):
        return self._create_log_shared(param, reference, hypothesis, 1, 3, "insert")

    def _create_log_hypothesis(self, param, hypothesis, reference):
        return self._create_log_shared(param, hypothesis, reference, 3, 1, "delete")
