\documentclass[11pt, titlepage]{article}
\title{\texttt{Intelligent Systems}\\Speech validator}
\author{
\-Wiebe \-Stassen \\*\\*
\-Robbin \-Siepman \\*\\*
}
\date{\today}
\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{float}
\usepackage{url}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{frame=tb,
  language=Python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3,
  numbers=left,
  numbersep=5pt,
  numberstyle=\tiny\color{gray},
  stepnumber=1,
  title=\lstname,
  rulecolor=\color{gray}
}
\usepackage{bera}% optional: just to have a nice mono-spaced font
\usepackage{listings}
\usepackage{xcolor}
\colorlet{punct}{red!60!black}
\definecolor{background}{HTML}{EEEEEE}
\definecolor{delim}{RGB}{20,105,176}
\colorlet{numb}{magenta!60!black}
\lstdefinelanguage{json}{
    basicstyle=\normalfont\ttfamily,
    numbers=left,
    numberstyle=\scriptsize,
    stepnumber=1,
    numbersep=8pt,
    showstringspaces=false,
    breaklines=true,
    frame=lines,
    backgroundcolor=\color{background},
    literate=
     *{0}{{{\color{numb}0}}}{1}
      {1}{{{\color{numb}1}}}{1}
      {2}{{{\color{numb}2}}}{1}
      {3}{{{\color{numb}3}}}{1}
      {4}{{{\color{numb}4}}}{1}
      {5}{{{\color{numb}5}}}{1}
      {6}{{{\color{numb}6}}}{1}
      {7}{{{\color{numb}7}}}{1}
      {8}{{{\color{numb}8}}}{1}
      {9}{{{\color{numb}9}}}{1}
      {:}{{{\color{punct}{:}}}}{1}
      {,}{{{\color{punct}{,}}}}{1}
      {\{}{{{\color{delim}{\{}}}}{1}
      {\}}{{{\color{delim}{\}}}}}{1}
      {[}{{{\color{delim}{[}}}}{1}
      {]}{{{\color{delim}{]}}}}{1},
}
\begin{document}
\maketitle
\tableofcontents
\newpage
\section{Introduction}
When validating speech recognition systems it is important that the metrics for validation are even for every system. This report will provide a description of a speech recognition validation system.
\noindent
\section{Interaction}
It is possible to interact with the speech validation system in two different ways, namely via a website (currently: \url{validator.robbit.me}) and via an api (currently \url{api.robbit.me}). 
\subsection{Website}
The website provides a friendly user interface, presented in figure \ref{website}. 
\begin{figure}[H]  
  \centering%
   \includegraphics[scale=0.22]{website.png}%
    \caption{Website.}
   \label{website}
\end{figure}
\noindent
Upon entering text in the website, there will be an internal call to the Api where the score shall be calculated. This score is then, once again, presented using a neat user interface (figure \ref{website_val}).
\begin{figure}[H]  
  \centering%
   \includegraphics[scale=0.22]{website_val.png}%
    \caption{Website.}
   \label{website_val}
\end{figure}
\noindent
In the validation view on the website you can see the Word Recall Rate (WRR) and the Word Error Rate (WER), these values give an indication on how accurate the recognized text was. Other information about the mistakes is also provided. The WRR is calculated by taking the correct words and dividing them by the original total of words. The WER is calculated by taking the number of errors and dividing them by the original number of words. This means you can have an WRR of 1 (perfect recall rate) but still have errors. For example, take the following sentence:
\begin{quote}
\texttt{Hello hello we are wiebe and robbin}
\end{quote}
\noindent
When this is validated against \texttt{hello we are wiebe and robbin} it will give a perfect WRR score (all words are correct), but the WER score will show that there was an error.
\noindent
Finally, in the website you can see an summary of where the errors were located. While this feature works most of the time, it can get confused if there are a significant number of errors, which is why we made a disclaimer for it (can be found on the website).
\subsection{Api}
When programming it is undesirable to use the friendly user interface because it will take up too much time and effort. In this case one should use the Api. The Api is called by performing a POST request to (currently) \texttt{api.robbit.me}, the text to be checked should be attached in JSON representation in the content header of the POST request. For example, take the following JSON file (1.json):
\begin{lstlisting}[language=json,firstnumber=1]
{
  "guess": "the quick brown fox jumped over the lazy dog",
  "reference" : "the quick brown fox jumped over the lazy dog"
}
\end{lstlisting}
\noindent
To then make the Api call, for example using curl:
\begin{lstlisting}
curl -d "@1.json" -X POST http://api.robbit.me/
\end{lstlisting}
\noindent
The server will process the request and answer the POST request with again a JSON string in the content header. A response will look similar to this:
\begin{lstlisting}[language=json,firstnumber=1]
{
  "deletions": 0,
  "hypothesis": "their quick brown fox jumped over the lazy dog",
  "insertions": 0,
  "reference_checked": "THE quick brown fox jumped over the lazy dog",
  "wer": 0.1111111111111111,
  "reference": "the quick brown fox jumped over the lazy dog",
  "wrr": 0.8888888888888888,
  "total_matches": 8,
  "hypothesis_checked": "THEIR quick brown fox jumped over the lazy dog",
  "replacements": 1,
  "total_errors": 1,
  "total_length": 9
}
\end{lstlisting}
\noindent
Just like with the user interface one can see the WER and WRR, information about errors and also a string representation of where the mistakes were made. The WER and WRR were calculated like so:
\begin{lstlisting}[language=json,firstnumber=1]
WER = 1 / 9 = 0.1111
WRR = 8 / 9 = 0.8889
\end{lstlisting}
\section{Api packets}
For ease of use there are a number of packets available.
\noindent
There multiple requests possible, requesting single sentences for validation
and multiple (arrays). For requesting single sentences, create a JSON string with the following fields:
\begin{lstlisting}[language=json,firstnumber=1]
{
  "guess": "stringtype",
  "reference": "stringtype",
}
\end{lstlisting}
\noindent
The validator will then check the guess against the reference. If you are usin one of the samples, you can simply provide the sample number index (with first sample at i=0) instead. The packet will then look like this:
\begin{lstlisting}[language=json,firstnumber=1] 
{
  "guess": "stringtype",
  "sample_id": 0
}
\end{lstlisting}
\noindent
A single request is not always desirable, therefore it is also possible to
request validation on an entire array. Like this:
\begin{lstlisting}[language=json,firstnumber=1]
{
  "guesses": ["stringtype", "stringtype", "stringtype"]
  "references": ["stringtype", "stringtype", "stringtype"]
}
\end{lstlisting}
\noindent
Here guess[0] is validated against reference[0] and so forth. Once again you 
can also just supply the sample id numbers if you are using the samples, like so:
\begin{lstlisting}[language=json,firstnumber=1]
{
  "guesses": ["stringtype", "stringtype", "stringtype"]
  "sample_ids": [0, 1, 2]
}
\end{lstlisting}
\noindent
In fact, you can leave out the sample ids altogether when using arrays, and 
the validator will assume that the first (index=0) guess corresponds with the first sample. So the packet underneath is the same as the packet above:
\begin{lstlisting}[language=json,firstnumber=1]
{
  "guesses": ["stringtype", "stringtype", "stringtype"]
}
\end{lstlisting}
\noindent
Note that the sample id cannot exceed 5, and when using a guesses only array 
you can only store a total of 6 guesses (0..5). But, if you provide the
sample\_id's you can pump as many requests as you want.
\section{Technicals}
The Api is made available using a RESTful server, using the Flask library. Everything is written in Python 3. The website mostly consists of PHP and performs an Api call to perform validation. The speech validator is based on another existing project, namely: \url{https://github.com/belambert/asr-evaluation}.
\section{Code}
Full source code, and more information, is available on: \\\url{https://gitlab.com/Robutil/SpeechValidator}.\\
\noindent
Visit \url{validator.robbit.me} (currently) to see everything in action.
\end{document}
