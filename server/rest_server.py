from flask import Flask, json, request, make_response
from flask_restful import Resource, Api

from validator.text_validator import TextValidator

app = Flask(__name__)
api = Api(app)


class SpeechRecognitionRestApi(Resource):
    def __init__(self):
        self.validator = TextValidator()

    def post(self):
        param = request.get_json(force=True)

        ret_object = '{}'
        if "guess" in param:
            ret_object = self._handle_single_request(param)
        elif "guesses" in param:
            ret_object = self._handle_multiple_requests(param)

        response = make_response(ret_object)
        response.headers['content-type'] = 'application/json'

        return response

    def _handle_single_request(self, param):
        guess = param['guess']

        ret_object = '{}'
        if "reference" in param:
            reference = param['reference']
            ret_object = self.validator.validate(guess, reference, parse_to_json=True)

        elif "sample_id" in param:
            sample_id = param['sample_id']
            ret_object = self.validator.validate_against_sample(guess, sample_id, parse_to_json=True)

        return ret_object

    def _handle_multiple_requests(self, param):
        guesses = param['guesses']

        ret_object = '{}'
        if 'references' in param:
            references = param['references']
            ret_object = json.dumps({'results': self.validator.validate_array(guesses, references)})

        elif 'sample_ids' in param:
            sample_ids = param['sample_ids']
            res = []

            if len(sample_ids) != len(guesses):
                return ret_object  # TODO: make this cleaner

            for i in range(len(sample_ids)):
                res.append(self.validator.validate_against_sample(guesses[i], sample_ids[i], parse_to_json=False))
            ret_object = json.dumps({'results': res})

        else:
            ret_object = self.validator.validate_against_samples(guesses, parse_to_json=True)

        return ret_object

    def get(self):
        return {'hello': 'world'}


api.add_resource(SpeechRecognitionRestApi, '/')

if __name__ == '__main__':
    app.run(debug=False)
